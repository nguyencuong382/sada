import React, {Component} from 'react';
import {Table, Avatar} from 'tabler-react';
import {connect} from 'react-redux';

class ListUser extends Component {
  componentDidMount() {
    // this.props.loadUser();
  }
  render() {
    return(
      <Table>
        <Table.Body>
          {
            this.props.players.map( (player, key) => (
              <Table.Row key={key}>
                <Table.Col ><Avatar imageURL={player.img} /></Table.Col>
                <Table.Col > {player.name}</Table.Col>
              </Table.Row>
            ))
          }
          
        </Table.Body>
      </Table>
    )
  }
}


const mapStateToProps = (state) => ({
  players: state.playerReducer.players,
});

const mapPropsToProps = dispatch => ({
  loadUser: () => dispatch({type: 'FETCH_ALL_PLAYERS'})
});

export default connect(
  mapStateToProps,
  mapPropsToProps,
)(ListUser);
