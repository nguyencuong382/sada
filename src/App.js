import React, { Component } from 'react';
// import logo from './logo.svg';
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from 'react-router-dom';
import './App.css';

import HomePage from './pages/home-page';
const supportsHistory = 'pushState' in window.history;

class App extends Component {
  render() {
    return (
      <Router forceRefresh={!supportsHistory}>  
        <Switch>
          <Redirect exact from="/" to="/dashboard" />
          <Route exact path="/dashboard" component={HomePage} />
        </Switch>
      </Router>
    );
  }
}

export default App;
