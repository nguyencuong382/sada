import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Site } from 'tabler-react';
import { connect } from 'react-redux';
// import md5 from 'md5';
import Loading from '../common/loading';

const navBarItems = [
  {
    value: 'Dashboard',
    to: '/dashboard',
    icon: 'home',
    LinkComponent: NavLink,
  },
  {
    value: 'Analytics',
    to: '/analytics',
    icon: 'file-text',
    LinkComponent: NavLink,
  },
  {
    value: 'Garbage Trucks',
    to: '/trucks',
    icon: 'truck',
    LinkComponent: NavLink,
  },
  {
    value: 'Garbage Bins',
    to: '/bins',
    icon: 'trash-2',
    LinkComponent: NavLink,
  },
  {
    value: 'Users',
    to: '/users',
    icon: 'users',
    LinkComponent: NavLink,
  },
  {
    value: 'Settings',
    to: '/settings',
    icon: 'settings',
    LinkComponent: NavLink,
  },
];

class DashboardLayout extends Component {


  render() {
    return (
      <Site.Wrapper
        headerProps={{
          href: '/dashboard',
          alt: 'Sada',
          imageURL: 'https://png2.kisspng.com/sh/4d3f7a7a321cfce89411ae41c13915d7/L0KzQYm3V8IxN6Z3gpH0aYP2gLBuTgNxd6N5Rd54Z3AwdrF2lPJidJ0ye95ycD3kgsW0kCBweqV4Rd54Z3B2PYbpVcFkbWo7eqRtNUC4PoK3UsM2O2U6Sac8MkC7R4q8WMc0OmYziNDw/kisspng-sport-logo-football-clip-art-sports-logos-5b51ce96b2d505.1023534515320879587325.png',
          accountDropdown: {
            avatarURL: 'https://www.gravatar.com/avatar/?d=monsterid',
            name: 'Nguyen Cuong',
            description: 'Administrator',
            options: [
              { icon: 'user', value: 'Profile' },
              { icon: 'settings', value: 'Settings' },
              { icon: 'mail', value: 'Inbox', badge: '6' },
              { icon: 'send', value: 'Message' },
              { isDivider: true },
              { icon: 'help-circle', value: 'Need help?' },
              { icon: 'log-out', value: 'Sign out', to: '/logout' },
            ],
          },
        }}
      >
      {this.props.children}
      </Site.Wrapper>
    )
  }
}

const mapStateToProps = (state) => ({
  // currUser: auth.currUser,
});

const mapPropsToProps = dispatch => ({
  // storeLoggedInUser: user => dispatch(storeLoggedInUserAction(user)),
});

export default connect(
  mapStateToProps,
  mapPropsToProps,
)(DashboardLayout);
