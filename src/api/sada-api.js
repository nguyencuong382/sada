import apisauce from 'apisauce';

const initialURL = 'http://localhost:9195';

const createSadaApi = (baseURL = initialURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json',
    },
    timeout: 5000
  });

  const fetchAllSada = () => api.get (
    '/sada',
  ).then(res => res.data);
  
  const fetchSadaByPlayer = (playerId, matchId) => api.get(
    `/sada/${playerId}/${matchId}`,
  ).then(res => res.data);
  
  const createSada = (data) => api.post(
    '/account',
    JSON.stringify(data)
  ).then(res => {
    if(res.status === 400) {
      throw new Error();
    }
    return res.data;
  })

  const updateLocation = (locationId, data) => api.put(
    `/location/${locationId}`,
    JSON.stringify(data)
  ).then(res => {
    if(res.status !== 200) {
      throw new Error();
    }
    return res.data});

  return {
    fetchAllSada,
    fetchSadaByPlayer,
    updateLocation,
    createSada,
  }
}

export default {
  createSadaApi
}