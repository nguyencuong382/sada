import apisauce from 'apisauce';

const initialURL = 'http://localhost:9195';

const createPlayerApi = (baseURL = initialURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Content-Type': 'application/json',
    },
    timeout: 5000
  });

  const fetchAllPlayers = () => api.get (
    '/player',
  ).then(res => res.data);

  return {
    fetchAllPlayers
  }
}

export default {
  createPlayerApi
}