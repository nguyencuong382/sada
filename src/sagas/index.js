
import createSadaApi from './../api/sada-api';
import createPlayerApi from './../api/player-api';

import {getAllSadaSaga} from './sada-saga';
import {getAllPlayers} from './player-saga';
import {fork, takeEvery} from 'redux-saga/effects';

const apiSada = createSadaApi.createSadaApi();
const apiPlayers = createPlayerApi.createPlayerApi();

function* watcher() {
  yield takeEvery('FETCH_ALL_SADA', getAllSadaSaga, apiSada);
  yield takeEvery('FETCH_ALL_PLAYERS', getAllPlayers, apiPlayers);
}

export default function* startForman() {
  yield fork(watcher);
}