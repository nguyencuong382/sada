import {put, call} from 'redux-saga/effects';
import action from '../redux/action';

export function* getAllPlayers(api) {
  try {
    const players = yield call(api.fetchAllPlayers);
    yield put(action.playerAction.loadAllPlayers(players));
  } catch (error) {
    console.log(error);
  }
}