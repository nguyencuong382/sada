import {put, call, select, take} from 'redux-saga/effects';
import action from '../redux/action';

// const getNewLocation = (state) => {
//   return state.reducerMap.locationUpdate;
// }


export function* getAllSadaSaga(api) {
  try {
    const sadas = yield call(api.fetchAllSada);
    yield put(action.cavasAction.getSadas(sadas));
  } catch (error) {
    console.log(error);
  }
}