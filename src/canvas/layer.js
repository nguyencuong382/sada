import {TIME_DELAY_ANIMATION} from './../constant';

window.Konva.Circle.prototype.moveTo = (pos, node) => {
  var tween = new window.Konva.Tween({
    node: node,
    duration: TIME_DELAY_ANIMATION/1000,
    x: pos.lat,
    y: pos.lng,
   
  });

  tween.play();
}

// console.log(new window.Konva.Circle());

var Layer = function() {
  var layer = new window.Konva.Layer();

  var getCircle = function (lat=10, lng=10, radius = 2, fill, stroke = 'white', strokeWidth = 0) {
    var circle = new window.Konva.Circle({
      x: lat,
      y: lng,
      radius: radius,
      fill: fill,
      stroke: stroke,
      strokeWidth: strokeWidth,
      type: 'circle',
    });
    return circle;
  }
  var addCircle = function(lat= 10, lng=10, radius = 5, fill, stroke = 'white', strokeWidth = 0) {
    var circle = getCircle(lat, lng, radius, fill, stroke, strokeWidth)
    layer.add(circle);
    return this;
  }

  var addLine = function(oldPos, newPos, stroke, strokeWidth = 3) {
    var redLine = new window.Konva.Line({
      points: [oldPos.lat, oldPos.lng, newPos.lat, newPos.lng],
      stroke: stroke,
      strokeWidth: strokeWidth,
      lineCap: 'round',
      lineJoin: 'round',
      type: 'line',
    });
    // console.log(redLine);
    layer.add(redLine);
    return this;
  }

  var getLayer = function() {
    return layer;
  }

  return {
    getCircle: getCircle,
    addCircle: addCircle,
    addLine: addLine,
    getLayer: getLayer
  }
}

export default Layer;