export const convertPositionToDataChar = async (sadas) => {
  var result = [];
  var runLoop =  () => {
    for(const sada of sadas) {
       result = [...result, {
        name: sada.time, v: sada.v, a: sada.a
      }]
    }
  }
  await runLoop();
 
  return result;
}