import React, {Component} from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import {connect} from 'react-redux';
import {convertPositionToDataChar} from './util-chart';
const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

class Chart extends Component {
  constructor(props) {
    super(props);
    this.data = []
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.isShowOne) {
      this.data = [...nextProps.sadas]
    } else {
      this.data = [...this.data, nextProps.position]
    }
  }

  render() {
    return(
      <LineChart width={950} height={300} data={this.data}
            margin={{top: 5, right: 0, left: 0, bottom: 5}}>
        <XAxis dataKey="time"/>
        <YAxis/>
        <CartesianGrid strokeDasharray="3 3"/>
        <Tooltip/>
        <Legend />
        <Line type="monotone" dataKey="v" stroke="#8884d8" activeDot={{r: 8}}/>
        <Line type="monotone" dataKey="a" stroke="#82ca9d" />
      </LineChart>
    )
  }
}

const mapStateToProps = state => ({
  sadas: state.canvasReducer.sadas,
  isShowOne: state.canvasReducer.isShowOne,
  position: state.canvasReducer.position,
});

export default connect(mapStateToProps, null)(Chart);