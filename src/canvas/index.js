import React, {Component} from 'react';
import CanvasDraw from './canvas';
import {connect} from 'react-redux';
import action from './../redux/action';
import {ACTION_CANVAS, SHOW_TYPE} from './../redux/action/canvas-action';
import createMapApi from './../api/sada-api';
import Chart from './chart';
import {Button} from 'tabler-react'
import {randomPos} from './../common/util';

class Canvas extends Component {
  constructor(props) {
    super(props);
    this.state = {val: 1};
  }

  addPoint = () => {
    this.props.addPoint(randomPos(this.props.position));
  }

  componentDidMount() {
    setInterval(() => {
      this.addPoint();
      // this.props.addReact();
    }, 2000);
  }

  

  render() {
    return(
      
        <CanvasDraw></CanvasDraw>
        // <Chart></Chart>
    )
  }
}

const mapStateToProps = state => ({
  position: state.canvasReducer.position,
  showType: state.canvasReducer.showType,
});

const mapDispatchToProps = dispatch => ({
  addPoint: (position) => dispatch(action.cavasAction.addPointAction(position)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Canvas);