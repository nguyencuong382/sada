import React, {Component} from 'react';
import {connect} from 'react-redux';
import Layer from './layer';
import './../style/canvas-style.css';
import {COLOR} from './../constant';
import {ACTION_CANVAS, SHOW_TYPE} from './../redux/action/canvas-action';
import {getHeight, getWidth} from './../common/util';
import {addCircle, toggleElement, hideLine, showLine} from './util-canvas';


class CanvasDraw extends Component {
  constructor(props) {
    super(props);
    this.bagCan = {
      stage: null,
      layer: null,
      startCircle: null,
      firstPos: {lat: 10, lng: 10},
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.log(nextProps)

    if(nextProps.isAddPoint) {
      switch (nextProps.actionType) {
        case ACTION_CANVAS.ADD_POINT: {
          var showLine = false;
          if(nextProps.showType === SHOW_TYPE.SHOW_ALL) {
            showLine = true;
          }
          addCircle(this.bagCan, nextProps.position, showLine);
          break;
        }
        default:
          break;
      }
    }
    
    if(nextProps.isChangeShow) {
      switch (nextProps.showType) {
        case SHOW_TYPE.SHOW_ALL:
          toggleElement(this.bagCan, true, true);
          break;
        case SHOW_TYPE.ONLY_POINT: {
          toggleElement(this.bagCan, true, false);
          break;
        }
        default:
          break;
      }
    }

    if(nextProps.isShowOne && nextProps.sadas.length > 0) {
      console.log(nextProps.sadas)
      this.bagCan.firstPos = nextProps.sadas[0];
      this.initScreen();
      this.startDrawing(nextProps.sadas);
    }

    
  }

  startDrawing =  (sadas) => {
    console.log(getHeight());
    console.log(getWidth());

    var runLoop = async () => {
      for(const sada of sadas) {
        sada.lat = getWidth()/105 * sada.lat;
        sada.lng = getHeight()/68 * sada.lng;
        await addCircle(this.bagCan, sada);
      }
    }

    runLoop().then();
  }

  initScreen = () => {
    var width = getWidth()
    var height = getHeight()
    this.bagCan.stage = new window.Konva.Stage({
      container: 'canvas_container',
      width: width,
      height: height
    });

    this.bagCan.startCircle = Layer().getCircle(this.bagCan.firstPos.lat, this.bagCan.firstPos.lng, 5, COLOR.GREEN);

    var layer = new window.Konva.Layer();
    layer.add(this.bagCan.startCircle);
    this.bagCan.layer = layer;
    this.bagCan.stage.add(layer);
  }

  componentDidMount() {
    this.initScreen();
  }

  render() {
    return(
      <div id="canvas_container"></div>
    )
  }
}

const mapStateToProps = state => ({
  position: state.canvasReducer.position,
  showType: state.canvasReducer.showType,
  actionType: state.canvasReducer.actionType,
  isChangeShow: state.canvasReducer.isChangeShow,
  isAddPoint: state.canvasReducer.isAddPoint,
  sadas: state.canvasReducer.sadas,
  isShowOne: state.canvasReducer.isShowOne,
});



export default connect(mapStateToProps, null)(CanvasDraw);