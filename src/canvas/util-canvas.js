import Layer from './layer';
import {COLOR, TIME_DELAY_ANIMATION} from './../constant';

export const hideLine = (layers) => {
  var shapes = layers.children;
  shapes.map( (shape, key) => {
    if(shape.attrs.type === 'line') {
      shape.hide();
    }
  })
}

export const showLine = layer => {
  var shapes = layer.children;
  shapes.map( (shape, key) => {
    if(shape.attrs.type === 'line') {
      shape.show();
    }
  })
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

export const addCircle = async (bagCan, pos, addWithLine = true) => {

  var oldPos = {lat: bagCan.startCircle.x(), lng: bagCan.startCircle.y()};

  bagCan.startCircle.moveTo(pos, bagCan.startCircle);

  
  
  await delay(TIME_DELAY_ANIMATION);


  var layer = Layer()
  .addLine(oldPos, pos, COLOR.GREEN)
  .addCircle(pos.lat, pos.lng, 5, COLOR.BLUE).getLayer();
  if(!addWithLine) {
    hideLine(layer);
  }
  bagCan.stage.add(layer);
  bagCan.layer.moveToTop();
}

export const toggleElement = (bagCan, isShowCircle = false, isShowLine = false) => {
  var layers = bagCan.stage.children; 
  if(isShowLine) {
    layers.map((layer, index) => {
      showLine(layer);
      layer.draw();
    })
  } else {
    layers.map((layer, index) => {
      hideLine(layer);
      layer.draw();
    })
  }  
};