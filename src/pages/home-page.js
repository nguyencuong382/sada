import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Tag, Icon, StampCard } from 'tabler-react';
import DashboardLayout from '../layouts/dash-board';
import Canvas from './../canvas';
import "tabler-react/dist/Tabler.css";
import './../style/dash-board.css';
import ListUser from './../component/user-list';
import Chart from './../canvas/chart';
import {ACTION_CANVAS, SHOW_TYPE} from './../redux/action/canvas-action';
import action from './../redux/action';
import {randomPos} from './../common/util';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.dash = {
      v: 0,
      s: 0,
      d: 0,
    }
  }

  addPoint = () => {
    this.props.addPoint(randomPos(this.props.position));
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    if(nextProps.actionType === ACTION_CANVAS.ADD_POINT) {
      var x = Math.pow(nextProps.position.lat - this.props.position.lat, 2 );
      var y = Math.pow(nextProps.position.lng - this.props.position.lng, 2);
      console.log(x + y)
      var sq = Math.sqrt(x + y) / (nextProps.position.time - this.props.position.time);
      this.dash.v = Math.round((this.dash.v + sq)*100/2)/100;
      this.dash.s = Math.round((this.dash.s + Math.sqrt(x + y)) * 100)/100;
      this.dash.d = Math.round((this.dash.s / 1370)*100) /100;
    }
  }

  render() {
    return(
      <DashboardLayout className="dash">

        <div className='row'>
          <div className="col-3 user">
          <ListUser></ListUser>
          </div>
          <div className='col-9'>
            <div className='row'>
              <div className="main-info">

              </div>
            </div>

            <div className='row'>
              <div className='match'>
                <div className='row'>
                  <div className='col-4'>
                  <StampCard prefix='fa' icon="dashboard" color="green">
                    {this.dash.v} km/h
                  </StampCard>

                  <StampCard prefix='fa' icon="dashboard" color="yellow">
                    {this.dash.s} m
                  </StampCard>

                  <StampCard prefix='fa' icon="dashboard" color="blue">
                    {this.dash.d} %
                  </StampCard>

                  </div>
                  <div className='col-8' >
                    <div className="row">
                      <div className='col-4'>
                          <Button color="success" onClick={this.addPoint} className='btn-canvase'>Current match</Button>
                          {this.props.showType === SHOW_TYPE.SHOW_ALL ? 
                            <Button color="secondary" onClick={this.props.hidePath} className='btn-canvase'>Hide path</Button> :
                            <Button color="secondary" onClick={this.props.showPath} className='btn-canvase'>Show path</Button>
                          }
                          <Button color="danger" onClick={this.props.showSada} className='btn-canvase'>Privious match</Button>
                      </div>

                      <div className='col-8' id='soccer-ba'>
                        <Canvas></Canvas>
                      </div>

                    </div>
                    
                  </div>
                </div>
                
              </div>
            </div>

            <div className='row'>
              <div className='chart-time'>
              <Chart></Chart>
              </div>
            </div>
          </div>
        </div>

        <Tag.List>
          <Tag addOn={<Icon name="user" />}>A User</Tag>
          <Tag color="danger" addOnIcon="activity">
            NPM
          </Tag>
          <Tag addOn="passing" addOnColor="success">
            tests
          </Tag>
          <Tag color="dark" addOn="1kb" addOnColor="warning">
            CSS gzip size
          </Tag>
        </Tag.List>
        
      </DashboardLayout>
    )
  }
}

const mapStateToProps = (state) => ({
  showType: state.canvasReducer.showType,
  position: state.canvasReducer.position,
  actionType: state.canvasReducer.actionType,
});

const mapDispatchToProps = dispatch => ({
  addPoint: (position) => dispatch(action.cavasAction.addPointAction(position)),
  hidePath: () => dispatch(action.cavasAction.hidePathAction()),
  showPath: () => dispatch(action.cavasAction.showPathAction()),
  showSada: () => dispatch({type: 'FETCH_ALL_SADA'}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
