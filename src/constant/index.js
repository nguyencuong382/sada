export const COLOR = {
  RED: '#D03526',
  GREEN: '#4B9400',
  BLUE: '#467FCF',
  YELLOW: '#F1C40F',
  ORANGE: '#FD9644',
};

export const TIME_DELAY_ANIMATION = 200;