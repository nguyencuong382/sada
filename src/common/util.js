

export const getWidth=()=> {
  var dpi_x = document.getElementById('dpi').offsetWidth;
  var w = document.getElementById('soccer-ba').offsetWidth;
  var width = w / dpi_x;
  return width;
}

export const getHeight=()=> {
  var dpi_y = document.getElementById('dpi').offsetHeight;
  var h = document.getElementById('soccer-ba').offsetHeight;
  var height = h / dpi_y;
  return height;
}


export const randomPos = (lastPosition) => {
  var min=0; 
  var max=20;  
  var randomV = Math.random() * (+max - +min) + +min; 


  min=0; 
  max=5; 
  var randomA = Math.random() * (+max - +min) + +min; 

  var radomB = Math.random() * (+max - +min) + +min;

  var x_add;

  if(radomB >= 0) {
    x_add= Math.random() * 10
  } else {
    x_add= -Math.random() * 10
  }

  var radomC = Math.random() * (+max - +min) + +min;

  var y_add; 
  if(radomC >= 0) {
    y_add= Math.random() * 10
  } else {
    y_add= -Math.random() * 10
  }

  var x = x_add + lastPosition.lat;
  var y = y_add + lastPosition.lng;

  var maxWith = document.getElementById('canvas_container').offsetWidth;
  var maxHeight = document.getElementById('canvas_container').offsetHeight;

  x = x > maxWith ? maxWith: x;
  y = y > maxHeight ? maxHeight:y;

  var position = {a: randomA, v: randomV, lat: x, lng: y, time: lastPosition.time + 1};

  return position;
}