import React, { Component } from 'react';

// eslint-disable-next-line react/prefer-stateless-function
class Loading extends Component {
  render() {
    const { type } = this.props;
    const getType = type || 'roller';
    return (
      <div className={`lds-${getType}`}>
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    );
  }
}

export default Loading;
