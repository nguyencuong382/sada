import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducer';
import createSageMiddleware from 'redux-saga';
import startForman from './../sagas';


const logAction = store => next => action => {
  console.log(action);
  console.log(store.getState());
  next(action);
}

const sageMiddleware = createSageMiddleware();

const store = createStore(reducer, applyMiddleware(logAction, thunk, sageMiddleware));
sageMiddleware.run(startForman);
// store.dispatch({type: 'FETCH_ALL_SADA'});


export default store;