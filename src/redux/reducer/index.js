import canvasReducer from './canvas-reducer';
import sadaReducer from './sada-reducer'
import playerReducer from './player-reducer';

import {combineReducers} from 'redux';

export default combineReducers({
  canvasReducer: canvasReducer,
  sadaReducer: sadaReducer,
  playerReducer: playerReducer,
});
