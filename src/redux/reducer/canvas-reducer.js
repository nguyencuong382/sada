import {ACTION_CANVAS, SHOW_TYPE} from './../action/canvas-action';

const initialCanvas = {
  position: {lat: 10, lng: 10, time: 1},
  showType: SHOW_TYPE.SHOW_ALL,
  actionType: null,
  sadas: [],
  isChangeShow: false,
  isAddPoint: false,
  isShowOne: false,
};

const canvasReducer = (state = initialCanvas, action) => {
  switch (action.type) {
    case ACTION_CANVAS.ADD_POINT:
      return {
        ...state,
        position: action.payload,
        actionType: ACTION_CANVAS.ADD_POINT,
        isChangeShow: false,
        isAddPoint: true,
        isShowOne: false,
      }
    case ACTION_CANVAS.HIDE_PATH: {
      return {
        ...state,
        actionType: ACTION_CANVAS.HIDE_PATH,
        showType: SHOW_TYPE.ONLY_POINT,
        isChangeShow: true,
        isAddPoint: false,
        isShowOne: false,
      }
    }
    case ACTION_CANVAS.SHOW_PATH: {
      return {
        ...state,
        actionType: ACTION_CANVAS.SHOW_PATH,
        showType: SHOW_TYPE.SHOW_ALL,
        isChangeShow: true,
        isAddPoint: false,
        isShowOne: false,
      }
    }
    case ACTION_CANVAS.SHOW_ONE:
      return {
        ...state,
        sadas: action.payload,
        isChangeShow: false,
        isAddPoint: false,
        isShowOne: true,
      }
    default:
      break;
  }

  return state;
}

export default canvasReducer;