import {ACTION_PLAYER} from './../action/player-action';

const initialState = {
  players: []
};

const playerReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_PLAYER.LOAD_ALL: {
      return {
        ...state,
        players: action.payload,
      }
    }
    default:
      break;
  }

  return state;
}

export default playerReducer;