export const ACTION_PLAYER = {
  LOAD_ALL: 'LOAD_ALL'
}



export const loadAllPlayers = (players) => {
  return {type: ACTION_PLAYER.LOAD_ALL, payload: players};
}

