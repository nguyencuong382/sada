import * as cavasAction from './canvas-action';
import * as sadaAction from './sada-action';
import * as playerAction from './player-action';

export default {
  cavasAction: cavasAction,
  sadaAction: sadaAction,
  playerAction: playerAction,
}