export const ACTION_CANVAS = {
  ADD_RECT: 'ADD_RECT',
  HIDE_PATH: 'HIDE_PATH',
  ADD_POINT: 'ADD_POINT',
  SHOW_PATH: 'SHOW_PATH',
  SHOW_ONE: 'SHOW_ONE',
}

export const SHOW_TYPE = {
  SHOW_ALL: 'SHOW_ALL',
  ONLY_POINT: 'ONLY_POINT',
  ONLY_PATH: 'ONLY_PATH',
}

export const hidePathAction = () => {
  console.log('aaa')
  return {type: ACTION_CANVAS.HIDE_PATH};
}

export const showPathAction = () => {
  return {type: ACTION_CANVAS.SHOW_PATH};
}

export const addPointAction = (position) => {
  return {type: ACTION_CANVAS.ADD_POINT, payload: position};
}

export const getSadas = data => {
  return {type: ACTION_CANVAS.SHOW_ONE, payload: data};
}
