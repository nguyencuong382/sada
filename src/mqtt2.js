import mqtt from 'mqtt';
import v4 from 'aws-signature-v4';
import crypto from 'crypto';

const IOT_ENDPOINT_HOST = 'as21qonfnbel7.iot.ap-southeast-1.amazonaws.com';
const IOT_ACCESS_KEY = 'AKIAJJCNL7KKISWX3NNQ';
const IOT_SECRET_KEY = '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';
const IOT_AWS_REGION = 'ap-southeast-1';

const getNotification = clientId => JSON.stringify({ clientId });

const validateClientConnected = (client) => {
  if (!client) {
    throw new Error('Client is not connected yet. Call client.connect() first!');
  }
};

export default (clientId, devices) => {
  const LAST_WILL_TOPIC = 'last-will';
  const MESSAGE_TOPIC = 'message';
  const CLIENT_CONNECTED = 'client-connected';
  const CLIENT_DISCONNECTED = 'client-disconnected';

  const url = v4.createPresignedURL(
    'GET',
    IOT_ENDPOINT_HOST.toLowerCase(),
    '/mqtt',
    'iotdevicegateway',
    crypto
      .createHash('sha256')
      .update('', 'utf8')
      .digest('hex'),
    {
      key: IOT_ACCESS_KEY,
      secret: IOT_SECRET_KEY,
      protocol: 'wss',
      region: IOT_AWS_REGION,
    },
  );

  const options = {
    will: {
      topic: LAST_WILL_TOPIC,
      payload: getNotification(clientId),
    },
  };
  let client = null;

  const clientWrapper = {};
  clientWrapper.connect = () => {
    client = mqtt.connect(
      url,
      options,
    );
    client.on('connect', () => {
      console.log('Connected to AWS IoT Broker');
      devices.forEach((device) => {
        client.subscribe(`metrics/${device}`);
      });
      client.subscribe(MESSAGE_TOPIC);
      client.subscribe(CLIENT_CONNECTED);
      client.subscribe(CLIENT_DISCONNECTED);
      const connectNotification = getNotification(clientId);
      client.publish(CLIENT_CONNECTED, connectNotification);
      console.log(`Sent message: ${CLIENT_CONNECTED} - ${connectNotification}`);
    });
    client.on('close', () => {
      console.log('Connection to AWS IoT Broker closed');
      client.end();
    });
  };

  clientWrapper.onConnect = (callback) => {
    validateClientConnected(client);
    client.on('connect', callback);
    return clientWrapper;
  };
  clientWrapper.onDisconnect = (callback) => {
    validateClientConnected(client);
    client.on('close', callback);
    return clientWrapper;
  };
  clientWrapper.onMessageReceived = (callback) => {
    validateClientConnected(client);
    client.on('message', (topic, message) => {
      // console.log(`Received message: ${topic} - ${message}`);
      const data = JSON.parse(message.toString('utf8'));
      // console.log('mppt data');
      // console.log(data);
      
    });
    return clientWrapper;
  };
  clientWrapper.sendMessage = (message) => {
    validateClientConnected(client);
    client.publish(MESSAGE_TOPIC, JSON.stringify(message));
    console.log(`Sent message: ${MESSAGE_TOPIC} - ${JSON.stringify(message)}`);
    return clientWrapper;
  };
  return clientWrapper;
};
